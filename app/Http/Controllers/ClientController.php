<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Client;
use App\Models\Quote;
use App\Models\OrderQuote;
use App\Services\CheckFormData;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $client_lists = DB::table('clients')
            ->select('id', 'name', 'ruby', 'created_at', 'email', 'phone', 'url')
            ->orderBy('id', 'asc')
            ->get();
        return view('client.index', compact('client_lists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('client.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Client;

        $data->name = $request->input('name');
        $data->ruby = $request->input('ruby');
        $data->zip = $request->input('zip');
        $data->state = $request->input('state');
        $data->city = $request->input('city');
        $data->address_1 = $request->input('address_1');
        $data->address_2 = $request->input('address_2');
        $data->phone = $request->input('phone');
        $data->fax = $request->input('fax');
        $data->mobile = $request->input('mobile');
        $data->email = $request->input('email');
        $data->url = $request->input('url');

        $data->save();

        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client_datum = Client::find($id);

        $state = CheckFormData::checkState($client_datum);

        return view('client.view', compact('client_datum', 'state'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
