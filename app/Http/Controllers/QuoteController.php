<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Client;
use App\Models\Quote;
use App\Services\CheckFormData;

class QuoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('quote.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $client_datum = Client::find($id);

        $state = CheckFormData::checkState($client_datum);

        return view('quote.new', compact('client_datum', 'state'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data1 = new Quote;

        $data1->quote_id = $request->input('quote_id');
        $data1->item = $request->input('item');
        $data1->quantity = $request->input('quantity');
        $data1->price = $request->input('price');
        $data1->item_discount = $request->input('item_discount');
        $data1->item_tax = $request->input('item_tax');
        $data1->text = $request->input('text');
        $data1->unit = $request->input('unit');

        $data1->save();

        $data2 = new OrderQuote;

        $data2->user_id = $request->input('user_id');
        $data2->discount = $request->input('discount');
        $data2->select_discount = $request->input('select_discount');
        $data2->select_tax = $request->input('select_tax');

        $data2->save();

        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
