<?php

namespace App\Services;

class CheckFormData
{
    public static function checkState($data)
    {
        if($data->state === 1){
            $state = '北海道';
        }
        if($data->state === 2){
            $state = '青森県';
        }
        if($data->state === 3){
            $state = '岩手県';
        }
        if($data->state === 4){
            $state = '宮城県';
        }
        if($data->state === 5){
            $state = '秋田県';
        }
        if($data->state === 6){
            $state = '山形県';
        }
        if($data->state === 7){
            $state = '福島県';
        }
        if($data->state === 8){
            $state = '茨城県';
        }
        if($data->state === 9){
            $state = '栃木県';
        }
        if($data->state === 10){
            $state = '群馬県';
        }
        if($data->state === 11){
            $state = '埼玉県';
        }
        if($data->state === 12){
            $state = '千葉県';
        }
        if($data->state === 13){
            $state = '東京都';
        }
        if($data->state === 14){
            $state = '神奈川県';
        }
        if($data->state === 15){
            $state = '新潟県';
        }
        if($data->state === 16){
            $state = '富山県';
        }
        if($data->state === 17){
            $state = '石川県';
        }
        if($data->state === 18){
            $state = '福井県';
        }
        if($data->state === 19){
            $state = '山梨県';
        }
        if($data->state === 20){
            $state = '長野県';
        }
        if($data->state === 21){
            $state = '岐阜県';
        }
        if($data->state === 22){
            $state = '静岡県';
        }
        if($data->state === 23){
            $state = '愛知県';
        }
        if($data->state === 24){
            $state = '三重県';
        }
        if($data->state === 25){
            $state = '滋賀県';
        }
        if($data->state === 26){
            $state = '京都府';
        }
        if($data->state === 27){
            $state = '大阪府';
        }
        if($data->state === 28){
            $state = '兵庫県';
        }
        if($data->state === 29){
            $state = '奈良県';
        }
        if($data->state === 30){
            $state = '和歌山県';
        }
        if($data->state === 31){
            $state = '鳥取県';
        }
        if($data->state === 32){
            $state = '島根県';
        }
        if($data->state === 33){
            $state = '岡山県';
        }
        if($data->state === 34){
            $state = '広島県';
        }
        if($data->state === 35){
            $state = '山口県';
        }
        if($data->state === 36){
            $state = '徳島県';
        }
        if($data->state === 37){
            $state = '香川県';
        }
        if($data->state === 38){
            $state = '愛媛県';
        }
        if($data->state === 39){
            $state = '高知県';
        }
        if($data->state === 40){
            $state = '福岡県';
        }
        if($data->state === 41){
            $state = '佐賀県';
        }
        if($data->state === 42){
            $state = '長崎県';
        }
        if($data->state === 43){
            $state = '熊本県';
        }
        if($data->state === 44){
            $state = '大分県';
        }
        if($data->state === 45){
            $state = '宮崎県';
        }
        if($data->state === 46){
            $state = '鹿児島県';
        }
        if($data->state === 47){
            $state = '沖縄県';
        }

        return $state;
    }
}
