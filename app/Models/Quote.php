<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    public function orderquote()
    {
        return $this->belongsTo('App\Models\OrderQuote');
    }
}
