<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderQuote extends Model
{
    public function client()
    {
        return $this->belongsTo('App\Models\Client');
    }

    public function quote()
    {
        return $this->hasMany('App\Models\Quote');
    }
}
