<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    public function orderinvoice()
    {
        return $this->belongsTo('App\Models\OrderInvoice');
    }
}
