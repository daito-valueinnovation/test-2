<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderInvoice extends Model
{
    public function client()
    {
        return $this->belongsTo('App\Models\Client');
    }

    public function invoice()
    {
        return $this->hasMany('App\Models\Invoice');
    }
}
