<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    public function client()
    {
        return $this->hasMany('App\Models\Client');
    }

    public function quote()
    {
        return $this->hasManyThrough('App\Models\Quote', 'App\Models\OrderQuote');
    }

    public function invoice()
    {
        return $this->hasManyThrough('App\Models\Invoice', 'App\Models\OrderInvoice');
    }
}
