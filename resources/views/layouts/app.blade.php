<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/select2.min.js') }}" defer></script>
    <script src="https://kit.fontawesome.com/00c90d0856.js" crossorigin="anonymous"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/add.css') }}" rel="stylesheet">

</head>

<body>
    <div id="app">
        <nav class="navbar navbar-expand-lg navbar-dark bg-nav shadow-sm">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="small">メニュー</span><span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-xl-auto ml-3">
                    <a class="navbar-brand" href="{{ url('/') }}">ホーム</a>
                </ul>
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav m-xl-auto">
                    <li class="nav-item dropdown pr-3">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-users pr-1"></i>取引先
                        </a>
                        <div class="dropdown-menu dropdown-menu-left">
                            <a class="dropdown-item" href="{{ route('client.new') }}">取引先追加</a>
                            <a class="dropdown-item" href="{{ route('client.index') }}">取引先一覧</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown pr-3">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-file pr-1"></i>見積書
                        </a>
                        <div class="dropdown-menu dropdown-menu-left">
                            <a class="btn dropdown-item" data-toggle="modal" data-target="#quoteModal">見積書作成</a>
                            <a class="dropdown-item" href="{{ route('quote.index') }}">見積書一覧</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown pr-3">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-file-alt pr-1"></i>請求書
                        </a>
                        <div class="dropdown-menu dropdown-menu-left">
                            <a class="btn dropdown-item" data-toggle="modal" data-target="#invoiceModal">請求書作成</a>
                            <a class="dropdown-item" href="{{ route('invoice.index') }}">請求書一覧</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown pr-3">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="far fa-credit-card pr-1"></i>入金一覧
                        </a>
                        <div class="dropdown-menu dropdown-menu-left">
                            <a class="dropdown-item" href="#">入金入力</a>
                            <a class="dropdown-item" href="#">入金一覧</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">オンライン入金ログ</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown pr-3">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-file-signature pr-1"></i>製品情報
                        </a>
                        <div class="dropdown-menu dropdown-menu-left">
                            <a class="dropdown-item" href="#">製品登録</a>
                            <a class="dropdown-item" href="#">製品一覧</a>
                            <a class="dropdown-item" href="#">製品区分</a>
                            <a class="dropdown-item" href="#">単位</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown pr-3">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-tasks pr-1"></i>タスク
                        </a>
                        <div class="dropdown-menu dropdown-menu-left">
                            <a class="dropdown-item" href="#">タスク作成</a>
                            <a class="dropdown-item" href="#">タスク一覧</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">プロジェクト作成</a>
                            <a class="dropdown-item" href="#">プロジェクト一覧</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown pr-3">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-calendar-alt pr-1"></i>帳票
                        </a>
                        <div class="dropdown-menu dropdown-menu-left">
                            <a class="dropdown-item" href="#">請求の時効</a>
                            <a class="dropdown-item" href="#">入金履歴</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">取引先別売上</a>
                            <a class="dropdown-item" href="#">日別売上</a>
                        </div>
                    </li>
                    <!-- <li class="nav-item dropdown pr-2">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                テスト
                            </a>
                            <div class="dropdown-menu dropdown-menu-left">
                                <a class="dropdown-item" href="#">テスト1</a>
                                <a class="dropdown-item" href="#">テスト2</a>
                                <a class="dropdown-item" href="#">テスト3</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">テスト4</a>
                            </div>
                        </li> -->
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-xl-auto">
                    <!-- Authentication Links -->
                    @guest
                    <li class="nav-item pr-2">
                        <a class="nav-link" href="{{ route('login') }}"><i class="fas fa-sign-in-alt pr-1"></i>{{ __('ログイン') }}</a>
                    </li>
                    @if (Route::has('register'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}"><i class="fas fa-user-plus pr-1"></i>{{ __('新規登録') }}</a>
                    </li>
                    @endif
                    @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                {{ __('ログアウト') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                    @endguest
                </ul>
            </div>
        </nav>

        <main>
            @yield('content')
        </main>
    </div>
        @include ('modal.search')
</body>

</html>