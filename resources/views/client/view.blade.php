@extends('layouts.app')

@section('head')
<!-- script -->

<!-- style -->
<link href="{{ asset('css/header/style.css') }}" rel="stylesheet">

@section('content')
<style>
    .thl-boder {
        border-bottom: 1px solid rgba(0, 0, 0, 0.125);
        border-right: 1px solid rgba(0, 0, 0, 0.125);
    }

    .thr-boder {
        border-bottom: 1px solid rgba(0, 0, 0, 0.125);
    }

    .tdl-boder {
        border-bottom: 1px solid rgba(0, 0, 0, 0.125);
        border-right: 1px solid rgba(0, 0, 0, 0.125);
    }

    .tdr-boder {
        border-bottom: 1px solid rgba(0, 0, 0, 0.125);
    }

    .td-hover tbody tr:hover {
        color: #212529;
        background-color: rgba(0, 0, 0, 0.025);
    }
</style>

<div style="background-color: #fff;">
    <div class="header-table shadow-sm py-3 mb-2">
        <p class="header-cell-left pl-4 h5">
            {{ $client_datum->name }}
        </p>
        <p class="header-cell-right pr-4">
            <button class="btn btn-sm btn-primary" onClick="location.href='/client/new'">
                <i class="fas fa-plus pr-2"></i>新規登録
            </button>
        </p>
    </div>
</div>

<ul class="nav nav-tabs" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="detail-tab" data-toggle="tab" href="#detail" role="tab" aria-controls="detail" aria-selected="true">
            詳細
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="quote-tab" data-toggle="tab" href="#quote" role="tab" aria-controls="quote" aria-selected="false">
            見積
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="invoice-tab" data-toggle="tab" href="#invoice" role="tab" aria-controls="invoice" aria-selected="false">
            請求
        </a>
    </li>
</ul>
<div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="detail" role="tabpanel" aria-labelledby="detail-tab">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6 py-2">
                    <div class="card">
                        <div class="card-header">
                            取引先詳細
                        </div>
                        <div class="card-body">
                            <table>
                                <thead>
                                    <tr>
                                        <th>住所</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>郵便番号</td>
                                        <td>
                                        {{ $client_datum->zip }}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <br>
                            {{ $state }}<br>
                            {{ $client_datum->city }}{{ $client_datum->city }}{{ $client_datum->address_1 }}<br>
                            {{ $client_datum->address_2 }}
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 py-2">
                    <div class="card">
                        <div class="card-header">
                            dummy data
                        </div>
                        <div class="card-body">
                            dummy data
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="quote" role="tabpanel" aria-labelledby="quote-tab">...</div>
    <div class="tab-pane fade" id="invoice" role="tabpanel" aria-labelledby="invoice-tab">...</div>
</div>
@endsection