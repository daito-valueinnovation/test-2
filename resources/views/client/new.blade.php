@extends('layouts.app')

@section('head')
<!-- script -->

<!-- style -->
<link href="{{ asset('css/header/style.css') }}" rel="stylesheet">

@section('content')
<script src="https://yubinbango.github.io/yubinbango/yubinbango.js" charset="UTF-8"></script>
<form method="POST" action="{{ route('client.store') }}" class="h-adr" autocomplete="off">
    @csrf
    <span class="p-country-name" style="display:none;">Japan</span>
    <div style="background-color: #fff;">
        <div class="header-table shadow-sm py-3 mb-2">
            <p class="header-cell-left pl-4 h5">
                取引先フォーム
            </p>
            <p class="header-cell-right pr-4">
                <button id="btn-submit" name="btn_submit" class="btn btn-sm btn-success" type="submit">
                    <i class="fa fa-check"></i> 保存
                </button>
                <button type="button" onclick="window.history.back()" id="btn-cancel" name="btn_cancel" class="btn btn-sm btn-danger" value="1">
                    <i class="fa fa-times"></i> キャンセル
                </button>
            </p>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6 py-2">
                <div class="card">
                    <div class="card-header">顧客情報</div>
                    <div class="card-body">
                        <label for="name">
                            顧客名
                        </label>
                        <input type="text" class="form-control" id="name" name="name" required="" autofocus="" value="" placeholder="入力必須">
                        <label for="ruby" class="mt-3">
                            読み仮名
                        </label>
                        <input type="text" class="form-control" id="ruby" name="ruby" autofocus="" value="" placeholder="省略可">
                    </div>
                </div>
            </div>
            <div class="col-sm-6 py-2" hidden>
                <div class="card">
                    <div class="card-header"></div>
                    <div class="card-body"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6 py-2">
                <div class="card">
                    <div class="card-header">住所</div>
                    <div class="card-body">
                        <label for="zip">
                            郵便番号（ハイフン不要）
                        </label>
                        <button type="button" class="btn btn-sm btn-primary postal-search">
                            住所検索
                        </button>
                        <input type="text" class="form-control p-postal-code" id="zip" name="zip" required="" autofocus="" value="" placeholder="入力必須">
                        <label for="state" class="mt-3">
                            都道府県
                        </label>
                        <select class="form-control p-region-id" id="state" name="state" autofocus="" value="">
                            <option value="">選択</option>
                            <option value="1">北海道</option>
                            <option value="2">青森県</option>
                            <option value="3">岩手県</option>
                            <option value="4">宮城県</option>
                            <option value="5">秋田県</option>
                            <option value="6">山形県</option>
                            <option value="7">福島県</option>
                            <option value="8">茨城県</option>
                            <option value="9">栃木県</option>
                            <option value="10">群馬県</option>
                            <option value="11">埼玉県</option>
                            <option value="12">千葉県</option>
                            <option value="13">東京都</option>
                            <option value="14">神奈川県</option>
                            <option value="15">新潟県</option>
                            <option value="16">富山県</option>
                            <option value="17">石川県</option>
                            <option value="18">福井県</option>
                            <option value="19">山梨県</option>
                            <option value="20">長野県</option>
                            <option value="21">岐阜県</option>
                            <option value="22">静岡県</option>
                            <option value="23">愛知県</option>
                            <option value="24">三重県</option>
                            <option value="25">滋賀県</option>
                            <option value="26">京都府</option>
                            <option value="27">大阪府</option>
                            <option value="28">兵庫県</option>
                            <option value="29">奈良県</option>
                            <option value="30">和歌山県</option>
                            <option value="31">鳥取県</option>
                            <option value="32">島根県</option>
                            <option value="33">岡山県</option>
                            <option value="34">広島県</option>
                            <option value="35">山口県</option>
                            <option value="36">徳島県</option>
                            <option value="37">香川県</option>
                            <option value="38">愛媛県</option>
                            <option value="39">高知県</option>
                            <option value="40">福岡県</option>
                            <option value="41">佐賀県</option>
                            <option value="42">長崎県</option>
                            <option value="43">熊本県</option>
                            <option value="44">大分県</option>
                            <option value="45">宮崎県</option>
                            <option value="46">鹿児島県</option>
                            <option value="47">沖縄県</option>
                        </select>
                        <label for="city" class="mt-3">
                            市区町村
                        </label>
                        <input type="text" class="form-control p-locality p-street-address" id="city" name="city" required="" autofocus="" value="" placeholder="入力必須">
                        <label for="address_1" class="mt-3">
                            番地
                        </label>
                        <input type="text" class="form-control" id="address_1" name="address_1" autofocus="" value="" placeholder="入力必須">
                        <label for="address_2" class="mt-3">
                            建物名
                        </label>
                        <input type="text" class="form-control p-extended-address" id="address_2" name="address_2" autofocus="" value="" placeholder="省略可">
                    </div>
                </div>
            </div>
            <div class="col-sm-6 py-2">
                <div class="card">
                    <div class="card-header">連絡先情報</div>
                    <div class="card-body">
                        <label for="phone">
                            電話番号
                        </label>
                        <input type="text" class="form-control" id="phone" name="phone" required="" autofocus="" value="" placeholder="入力必須">
                        <label for="fax" class="mt-3">
                            FAX番号
                        </label>
                        <input type="text" class="form-control" id="fax" name="fax" autofocus="" value="" placeholder="省略可">
                        <label for="mobile" class="mt-3">
                            携帯番号
                        </label>
                        <input type="text" class="form-control" id="mobile" name="mobile" autofocus="" value="" placeholder="省略可">
                        <label for="email" class="mt-3">
                            メールアドレス
                        </label>
                        <input type="email" class="form-control" id="email" name="email" autofocus="" value="" placeholder="省略可">
                        <label for="url" class="mt-3">
                            ホームページ
                        </label>
                        <input type="url" class="form-control" id="url" name="url" autofocus="" value="" placeholder="省略可">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid" hidden>
        <div class="row">
            <div class="col-sm-6 py-2">
                <div class="card">
                    <div class="card-header">税金情報</div>
                    <div class="card-body">
                        ----- test -----
                    </div>
                </div>
            </div>
            <div class="col-sm-6 py-2">
                <div class="card">
                    <div class="card-header">Dashboard</div>
                    <div class="card-body">
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<script src="{{ asset('js/client/zipcancel.js') }}" defer></script>
@endsection