@extends('layouts.app')

@section('head')
<!-- script -->

<!-- style -->
<link href="{{ asset('css/header/style.css') }}" rel="stylesheet">

@section('content')

<link href="{{ asset('css/item/body.css') }}" rel="stylesheet">

<div style="background-color: #fff;">
    <div class="header-table shadow-sm py-3 mb-2">
        <p class="header-cell-left pl-4 h5">
            取引先一覧
        </p>
        <p class="header-cell-right pr-4">
            <button class="btn btn-sm btn-primary" onClick="location.href='/client/new'">
                <i class="fas fa-plus pr-2"></i>新規登録
            </button>
        </p>
    </div>
</div>

<div class="container-fluid m-1">
    <table class="td-hover th-bg" width="100%">
        <thead class="text-center small">
            <th class="thl-boder" width="5%">id</th>
            <th class="thl-boder" width="25%">顧客名</th>
            <th class="thl-boder" width="10%">電話番号</th>
            <th class="thl-boder" width="15%">メールアドレス</th>
            <th class="thl-boder" width="20%">ホームページ</th>
            <th class="thl-boder" width="10%">登録日</th>
            <th class="thr-boder" width="15%"></th>
        </thead>
        @foreach($client_lists as $client_list)
        <tbody class="small">
            <tr>
                <td class="tdl-boder text-center">{{ $client_list->id }}</td>
                <td class="tdl-boder text-center"><a href="{{ route('client.view', ['id' => $client_list->id ]) }}">{{ $client_list->name }}</a></td>
                <td class="tdl-boder text-center"><a href="tel:{{ $client_list->phone }}">{{ $client_list->phone }}</a></td>
                <td class="tdl-boder text-center"><a href="mailto:{{ $client_list->email }}">{{ $client_list->email }}</a></td>
                <td class="tdl-boder"><a class="ml-3" href="{{ $client_list->url }}">{{ $client_list->url }}</a></td>
                <td class="tdl-boder text-center">{{ $client_list->created_at }}</td>
                <td class="tdr-boder">
                    <div class="dropdown">
                        <button class="btn btn-sm btn-outline-secondary dropdown-toggle ml-3 my-1" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-cog pr-2"></i><span class="small">オプション</span>
                        </button>
                        <div class="dropdown-menu dropdown-menu-left" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="{{ route('client.new') }}">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</div>
@endforeach

@endsection