<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8" />
    <title>ドラッグ＆ドロップで並べ替えるときのドラッグできる部分を指定</title>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script>
$( function() {
    $( '#sortable' ) . sortable( {
        handle: 'button',
        cancel: ''
    } );
} );
    </script>


</head>

<body>
<div id="sortable">
    <div><button>+</button>項目 1</div>
    <div><button>+</button>項目 2</div>
    <div><button>+</button>項目 3</div>
    <div><button>+</button>項目 4</div>
    <div><button>+</button>項目 5</div>
    <div><button>+</button>項目 6</div>
    <div><button>+</button>項目 7</div>
</div>
</body>

</html>