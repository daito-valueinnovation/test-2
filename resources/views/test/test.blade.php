@extends('layouts.app')

@section('content')
<style>
    .header-table {
        display: table;
        width: 100%;
        background-color: rgba(0, 0, 0, 0.03);
    }

    .header-cell-left {
        display: table-cell;
        text-align: left;
    }

    .header-cell-right {
        display: table-cell;
        text-align: right;
    }
</style>

<div style="background-color: #fff;">
    <div class="header-table shadow-sm py-3 mb-2">
        <p class="header-cell-left pl-4 h5">
            dummy data
        </p>
        <p class="header-cell-right pr-4">
            <button id="btn-submit" name="btn_submit" class="btn btn-sm btn-success" value="1">
                <i class="fa fa-check"></i> 保存
            </button>
            <button type="button" onclick="window.history.back()" id="btn-cancel" name="btn_cancel" class="btn btn-sm btn-danger" value="1">
                <i class="fa fa-times"></i> キャンセル
            </button>
        </p>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-6 py-2">
            <div class="card">
                <div class="card-header">
                    dummy data
                </div>
                <div class="card-body">
                    dummy data
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 py-2">
            <div class="card">
                <div class="card-header">
                    dummy data
                </div>
                <div class="card-body">
                    dummy data
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-6 py-2">
            <div class="card">
                <div class="card-header">
                    dummy data
                </div>
                <div class="card-body">
                    dummy data
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 py-2">
            <div class="card">
                <div class="card-header">
                    dummy data
                </div>
                <div class="card-body">
                    dummy data
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-6 py-2">
            <div class="card">
                <div class="card-header">
                    dummy data
                </div>
                <div class="card-body">
                    dummy data
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 py-2">
            <div class="card">
                <div class="card-header">
                    dummy data
                </div>
                <div class="card-body">
                    dummy data
                </div>
            </div>
        </div>
    </div>
</div>
@endsection