@extends('layouts.app')

@section('head')
<!-- font -->
<link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

<!-- style -->
<link href="{{ asset('css/top.css') }}" rel="stylesheet">

@section('content')
<!-- bootstrap overwrite style -->
<link href="{{ asset('css/quick_action/style.css') }}" rel="stylesheet">

<div class="p-3 d-none d-sm-block" draggable="true">
    <div class="row px-3">
            <div class="custom-header col-sm-12 font-weight-bold py-2">クイックアクション</div>
</div>
    <div class="row px-3">
        <div class="btn custom-bg custom-body-left col-sm-3 m-hover" onClick="location.href='/client/new'">
            <i class="fas fa-user-plus pr-2"></i>取引先追加
        </div>
        <div class="btn custom-bg custom-body-center col-sm-3 m-hover" data-toggle="modal" data-target="#quoteModal">
            <i class="fas fa-file pr-2"></i>見積書作成
        </div>
        <div class="btn custom-bg custom-body-center col-sm-3 m-hover" data-toggle="modal" data-target="#invoiceModal">
            <i class="fas fa-file-alt pr-2"></i>請求書作成
        </div>
        <div class="btn custom-bg custom-body-right rdr col-sm-3 m-hover" onClick="location.href='/'">
            <i class="far fa-credit-card pr-2"></i>入金入力
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-6 py-2">
            <div class="card">
                <div class="card-header">
                    dummy data
                </div>
                <div class="card-body">
                    dummy data
                </div>
            </div>
        </div>
        <div class="col-sm-6 py-2">
            <div class="card">
                <div class="card-header">
                    dummy data
                </div>
                <div class="card-body">
                    dummy data
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-6 py-2">
            <div class="card">
                <div class="card-header">
                    dummy data
                </div>
                <div class="card-body">
                    dummy data
                </div>
            </div>
        </div>
        <div class="col-sm-6 py-2">
            <div class="card">
                <div class="card-header">
                    dummy data
                </div>
                <div class="card-body">
                    dummy data
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-6 py-2">
            <div class="card">
                <div class="card-header">
                    dummy data
                </div>
                <div class="card-body">
                    dummy data
                </div>
            </div>
        </div>
        <div class="col-sm-6 py-2">
            <div class="card">
                <div class="card-header">
                    dummy data
                </div>
                <div class="card-body">
                    dummy data
                </div>
            </div>
        </div>
    </div>
</div>

<div class="flex-center position-ref full-height">

    <div class="content">
        <div class="title m-b-md">
            Laravel
        </div>

        <div class="links">
            <a href="https://laravel.com/docs">Docs</a>
            <a href="https://laracasts.com">Laracasts</a>
            <a href="https://laravel-news.com">News</a>
            <a href="https://blog.laravel.com">Blog</a>
            <a href="https://nova.laravel.com">Nova</a>
            <a href="https://forge.laravel.com">Forge</a>
            <a href="https://vapor.laravel.com">Vapor</a>
            <a href="https://github.com/laravel/laravel">GitHub</a>
        </div>
    </div>
</div>
@endsection