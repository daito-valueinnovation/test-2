@extends('layouts.app')

@section('head')
<!-- script -->
<script src="{{ asset('js/item/clone.js') }}"></script>
<script src="{{ asset('js/item/sortable.min.js') }}"></script>

<!-- style -->
<link href="{{ asset('css/header/style.css') }}" rel="stylesheet">

@section('content')

<link href="{{ asset('css/item/item.css') }}" rel="stylesheet">

<form method="POST" action="{{ route('quote.store') }}" autocomplete="off">
    @csrf
    <input name="quote_id" type="text" value="{{ $client_datum->id }}" hidden>
    <div style="background-color: #fff;">
        <div class="header-table shadow-sm py-3 mb-2">
            <p class="header-cell-left pl-4 h5">
                見積書作成
            </p>
            <p class="header-cell-right pr-4">
                <button id="btn-submit" name="btn_submit" class="btn btn-sm btn-success" type="submit">
                    <i class="fa fa-check"></i> 保存
                </button>
                <button type="button" onclick="window.history.back()" id="btn-cancel" name="btn_cancel" class="btn btn-sm btn-danger" value="1">
                    <i class="fa fa-times"></i> キャンセル
                </button>
            </p>
        </div>
    </div>

    <div class="container-fluid" draggable="true">
        <div class="m-1">
            <div class="row">
                <div class="col-md-6 py-2">
                    <div class="card">
                        <div class="card-header">顧客情報</div>
                        <div class="card-body">
                            顧客名
                        </div>
                    </div>
                </div>
                <div class="col-md-6 py-2">
                    <div class="card">
                        <div class="card-header">dummy data</div>
                        <div class="card-body">test</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid" onclick="compute()" onfocus="compute()" onkeyup="compute()">
        <div class="m-1">
            <div class="card">
                <div class="card-header">品目</div>
                <div class="card-body">
                    <span class="btn btn-outline-secondary addBtn mx-2 mb-2" type="button"><i class="fas fa-plus pr-2"></i>新しい行の追加</span>
                    <div class="table-responsive">
                        <table class="additem lg-width">
                            <tbody id="sort">
                                <?php
                                $count = 1;
                                for ($i = 0; $i < $count; $i++) {
                                ?>
                                    <tr style="display: none;">
                                        <td>
                                            <div class="container-fluid">
                                                <div class="row flex-nowrap">
                                                    <div class="custom-up-left d-flex align-items-center justify-content-center">
                                                        <span class="handle mouse-move"><i class="fas fa-sort"></i></span>
                                                    </div>
                                                    <div class="custom-up-center-l">
                                                        <div class="mx-3">
                                                            <div class="input-group input-group-sm my-1">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text">項目</span>
                                                                </div>
                                                                <input id="item_<?= $i ?>" class="form-control" name="item[]" type="text">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="custom-up-center-r col-md">
                                                        <div class="input-group input-group-sm my-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">数量</span>
                                                            </div>
                                                            <input id="quantity_<?= $i ?>" class="form-control number tar" name="quantity[]" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="custom-up-center-r col-md">
                                                        <div class="input-group input-group-sm my-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">価格</span>
                                                            </div>
                                                            <input id="price_<?= $i ?>" class="form-control number tar" name="price[]" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="custom-up-center-r col-md">
                                                        <div class="input-group input-group-sm my-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">項目割引</span>
                                                            </div>
                                                            <input id="item_discount_<?= $i ?>" class="form-control number tar" name="item_discount[]" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="custom-up-center-r col-md">
                                                        <div class="input-group input-group-sm my-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">税種</span>
                                                            </div>
                                                            <select id="item_tax_<?= $i ?>" class="form-control custom-select" name="item_tax[]" type="text">
                                                                <option value="1">なし</option>
                                                                <option value="2">8%</option>
                                                                <option value="3">10%</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="custom-up-right d-flex align-items-center justify-content-end col-md">
                                                        <span class="btnDelete" type="button"><i class="fas fa-trash-alt"></i></span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="custom-down-left">
                                                    </div>
                                                    <div class="custom-down-center-l">
                                                        <div class="mx-3">
                                                            <div class="input-group input-group-sm mt-1">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text">備考</span>
                                                                </div>
                                                                <textarea id="text_<?= $i ?>" class="form-control" name="text[]" type="text"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="custom-down-center-r col-md">
                                                        <div class="input-group input-group-sm my-1">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">単位</span>
                                                            </div>
                                                            <input id="unit_<?= $i ?>" class="form-control" name="unit[]" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="custom-down-center-r col-md">
                                                        <div class="d-flex align-items-end justify-content-end mx-2 my-1">
                                                            <span class="small">小計</span>
                                                        </div>
                                                        <div class="input-group-sm mb-1 mx-1">
                                                            <input id="subtotal_<?= $i ?>" class="form-control number tar" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="custom-down-center-r col-md">
                                                        <div class="d-flex align-items-end justify-content-end mx-2 my-1">
                                                            <span class="small">割引額</span>
                                                        </div>
                                                        <div class="input-group-sm mb-1 mx-1">
                                                            <input id="d_amount_<?= $i ?>" class="form-control number tar" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="custom-down-center-r col-md">
                                                        <div class="d-flex align-items-end justify-content-end mx-2 my-1">
                                                            <span class="small">税額</span>
                                                        </div>
                                                        <div class="input-group-sm mb-1 mx-1">
                                                            <input id="tax_amount_<?= $i ?>" class="form-control number tar" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="custom-down-right col-md">
                                                        <div class="d-flex align-items-end justify-content-end mx-2 my-1">
                                                            <span class="small">合計</span>
                                                        </div>
                                                        <div class="input-group-sm mb-1 mx-1">
                                                            <input id="total_<?= $i ?>" class="form-control number tar" type="text" value="">
                                                            <span id="stotal_<?= $i ?>" class="item_total" value=""></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
                            </form>
    <div class="container-fluid">
        <div class="m-3">
            <div class="row d-flex justify-content-end" hidder>
                <div class="col-md-6 py-2">
                    <div class="card">
                        <div class="card-header">税金情報</div>
                        <div class="card-body">
                        </div>
                    </div>
                </div>
                <div class="col-md-6 py-2">
                    <div class="d-flex align-items-center justify-content-end">
                        <table class="table-boder">
                            <tbody>
                                <tr>
                                    <td class="table-boder small">
                                        <div class="text-right mx-2">
                                            小計
                                        </div>
                                    </td>
                                    <td class="table-boder">
                                        <div class="input-group-sm my-1 mx-4">
                                            <input id="subtotal" class="form-control number tar" type="text">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="table-boder small">
                                        <div class="text-right mx-2">
                                            項目税
                                        </div>
                                    </td>
                                    <td class="table-boder">
                                        <div class="input-group-sm my-1 mx-4">
                                            <input id="tax_sum" class="form-control number tar" type="text">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="table-boder small">
                                        <div class="text-right mx-2">
                                            割引
                                        </div>
                                    </td>
                                    <td class="table-boder">
                                        <div class="row mx-4">
                                            <div class="input-group-sm my-1">
                                                <input id="discount" name="discount" class="form-control number tar" type="text">
                                            </div>
                                            <div class="input-group-sm my-1">
                                                <select id="select_discount" name="select_discount" class="form-control custom-select" type="text" onclick="compute()">
                                                    <option value="1">---</option>
                                                    <option value="2">%</option>
                                                    <option value="3">¥</option>
                                                </select>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <td class="table-boder small">
                                    <div class="text-right mx-2">
                                        割引額
                                    </div>
                                </td>
                                <td class="table-boder">
                                    <div class="input-group-sm my-1 mx-4">
                                        <input id="d_amount" class="form-control number tar" type="text">
                                    </div>
                                </td>
                                </tr>
                                <tr>
                                    <td class="table-boder">
                                        <div class="input-group input-group-sm my-1 px-1">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">税</span>
                                            </div>
                                            <select id="select_tax" name="select_tax" class="form-control custom-select" type="text">
                                                <option value="1">消費税10%</option>
                                                <option value="2">消費税8%</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td class="table-boder">
                                        <div class="input-group-sm my-1 mx-4">
                                            <input id="tax_amount" class="form-control number tar" type="text">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="table-boder small">
                                        <div class="text-right mx-2">
                                            合計
                                        </div>
                                    </td>
                                    <td class="table-boder">
                                        <div class="input-group-sm my-1 mx-4">
                                            <input id="total" class="form-control number tar" type="text">
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="m-1">
            <div class="row d-flex justify-content-end">
                <div class="col-md-6 py-2">
                    <div class="card">
                        <div class="card-header">dummy data</div>
                        <div class="card-body">
                            ----- test -----
                        </div>
                    </div>
                </div>
                <div class="col-md-6 py-2">
                    <div class="card">
                        <div class="card-header">dummy data</div>
                        <div class="card-body">
                            ----- test -----
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script src="{{ asset('js/item/sortable.js') }}"></script>
<script src="{{ asset('js/item/format.js') }}"></script>
<script src="{{ asset('js/item/computes.js') }}"></script>
@include ('layouts.input')
@endsection