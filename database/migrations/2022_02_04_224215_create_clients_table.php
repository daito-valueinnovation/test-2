<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 30);
            $table->string('ruby', 30);
            $table->string('zip', 7);
            $table->tinyInteger('state');
            $table->string('city', 30);
            $table->string('address_1', 50);
            $table->string('address_2', 50)->nullable();
            $table->string('phone', 15);
            $table->string('fax', 15)->nullable();
            $table->string('mobile', 15)->nullable();
            $table->string('email', 255)->nullable();
            $table->longText('url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
