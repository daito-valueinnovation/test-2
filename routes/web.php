<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/modal', function () {
    return view('modal.search');
});

Route::get('/test', function () {
    return view('test.test');
});

Route::get('select2_ajax', 'ClientNameController@select2_ajax');
Route::get('ajax/client', 'Ajax\UserController@index');


Route::group(['prefix' => 'test', 'middleware' => 'auth'], function(){
    Route::get('index', 'TestController@index')->name('test.index');
    Route::get('new', 'TestController@new')->name('test.new');
});

Route::group(['prefix' => 'client', 'middleware' => 'auth'], function(){
    Route::get('index', 'ClientController@index')->name('client.index');
    Route::get('new', 'ClientController@create')->name('client.new');
    Route::post('store', 'ClientController@store')->name('client.store');
    Route::get('view/{id}', 'ClientController@show')->name('client.view');
});

Route::group(['prefix' => 'quote', 'middleware' => 'auth'], function(){
    Route::get('index', 'QuoteController@index')->name('quote.index');
    Route::get('new/{id}', 'QuoteController@create')->name('quote.new');
    Route::post('store', 'QuoteController@store')->name('quote.store');
});

Route::group(['prefix' => 'invoice', 'middleware' => 'auth'], function(){
    Route::get('index', 'InvoiceController@index')->name('invoice.index');
    Route::get('new', 'InvoiceController@create')->name('invoice.new');
    Route::post('store', 'InvoiceController@store')->name('invoice.store');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
