window.onload = function () {
    $(".number").each(function () {
        addFigure(this);
    });
    $(".number").focus(function () {
        delFigure(this);
    });
    $(".number").blur(function () {
        addFigure(this);
    });
    $("#save").click(function () {
        $(".number").each(function () {
            delFigure(this);
        });
    });
    function addFigure(query) {
        $(query).val($(query).val().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,'));
    }
    function delFigure(query) {
        $(query).val($(query).val().replace(/,/g, ''));
    }
}