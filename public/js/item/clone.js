window.addEventListener('DOMContentLoaded', function () {
    $(function () {
        $('.addBtn').click(function (e) {
            var cnt = $('.additem [id^=item_]').length;
            var dom = $('.additem tbody tr:first-child').clone(true).appendTo(".additem tbody");
            dom.find('input[id]').each(function () {
                var id = $(this).attr('id').replace(/_([0-9]+)$/g, '_' + cnt);
                $(this).attr('id', id);
            });
            dom.find('select[id]').each(function () {
                var id = $(this).attr('id').replace(/_([0-9]+)$/g, '_' + cnt);
                $(this).attr('id', id);
            });
            dom.find('textarea[id]').each(function () {
                var id = $(this).attr('id').replace(/_([0-9]+)$/g, '_' + cnt);
                $(this).attr('id', id);
            });
            dom.find('span[id]').each(function () {
                var id = $(this).attr('id').replace(/_([0-9]+)$/g, '_' + cnt);
                $(this).attr('id', id);
            });
            dom.find('label[for]').each(function () {
                var fr = $(this).attr('for').replace(/_([0-9]+)$/g, '_' + cnt);
                $(this).attr('for', fr);
            });
            dom.find('input[name]').each(function () {
                var name = $(this).attr('name').replace(/_([0-9]+)$/g, '_' + cnt);
                $(this).attr('name', name);
            });
            dom.find('select[name]').each(function () {
                var name = $(this).attr('name').replace(/_([0-9]+)$/g, '_' + cnt);
                $(this).attr('name', name);
            });
            dom.find('textarea[name]').each(function () {
                var name = $(this).attr('name').replace(/_([0-9]+)$/g, '_' + cnt);
                $(this).attr('name', name);
            });

            dom.find('input').val('')

            dom.appendTo($('.additem'));

            $(".additem tbody tr:last-child").css("display", "table-row");

            $(".btnDelete").on("click", function () {
                $(this).parent().parent().parent().remove();
            });
        });
    });
});