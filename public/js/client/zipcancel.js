(function() {
    var hadr = document.querySelector(".h-adr"),
        cancelFlag = true;

    var onKeyupCanceller = function(e) {
        if(cancelFlag){
            e.stopImmediatePropagation();
        }
        return false;
    };

    var postalcode = hadr.querySelectorAll(".p-postal-code"),
        postalField = postalcode[postalcode.length - 1];

    postalField.addEventListener("keyup", onKeyupCanceller, false);

    var btn = hadr.querySelector(".postal-search");
    btn.addEventListener("click", function(e) {

        cancelFlag = false;

        let event;
        if (typeof Event === "function") {
            event = new Event("keyup");
        } else {
            event = document.createEvent("Event");
            event.initEvent("keyup", true, true);
        }
        postalField.dispatchEvent(event);

        cancelFlag = true;
    });
})();